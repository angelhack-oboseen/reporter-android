package obo.seen.oboseen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by JR Aquino on 7/2/2017.
 */

public class AccountRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int IMAGE_VIEW_TYPE = 0;
    private static final int NAME_VIEW_TYPE = 1;
    private static final int OPTION_VIEW_TYPE = 2;
    private String mUsername;

    public AccountRecyclerViewAdapter (String username) {
        mUsername = username;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == IMAGE_VIEW_TYPE) {
            View view = LayoutInflater
                    .from(context)
                    .inflate(R.layout.user_avatar, parent, false);
            return new ViewHolder(view);
        } else if (viewType == NAME_VIEW_TYPE) {
            View view = LayoutInflater
                    .from(context)
                    .inflate(R.layout.user_name, parent, false);
            return new NameViewHolder(view);
        } else {
            View view = LayoutInflater
                    .from(context)
                    .inflate(R.layout.list_option, parent, false);
            return new OptionViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 1) {
            Log.d("debugdebugdebug", "bind username");
            NameViewHolder vh = (NameViewHolder) holder;
            vh.name.setText(mUsername);
        } else if (position == 2) {
            Log.d("debugdebugdebug", "bind log out");
            OptionViewHolder vh = (OptionViewHolder) holder;
            vh.name.setText("Log out");
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0
                ? IMAGE_VIEW_TYPE
                : position == 1
                    ? NAME_VIEW_TYPE
                    : OPTION_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class NameViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        public NameViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
        }
    }

    public class OptionViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        public OptionViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_option_name);
        }
    }
}
