package obo.seen.oboseen.request.error;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import obo.seen.oboseen.request.CustomRequest;


/**
 * Created by JR Aquino on 5/21/2017.
 */
public class AuthenticationRequestErrorParser implements CustomRequest.NetworkErrorParser {
    @Override
    public VolleyError parseNetworkError(VolleyError volleyError) {
        if (volleyError.networkResponse != null) {
            String jsonResponse = new String(volleyError.networkResponse.data);
            JSONObject parsedResponse;
            String message;
            try {
                parsedResponse = new JSONObject(jsonResponse);
                message = parsedResponse.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
                return new VolleyError(volleyError.networkResponse);
            }
            Log.d("debug", message);
            if (volleyError.networkResponse.statusCode == 400) {
                if (message.equals("Invalid grant: user credentials are invalid") ||
                        message.equals("Invalid grant: refresh token is invalid") ||
                        message.equals("Missing parameter: `username`") ||
                        message.equals("Missing parameter: `password`")) {
                    return new AuthFailureError(volleyError.networkResponse);
                } else if (message.equals("Invalid client: client is invalid") ||
                        message.equals("Missing parameter: `grant_type`")) {
                    return new ServerError(volleyError.networkResponse);
                }
            }
        }
        return volleyError;
    }
}
