package obo.seen.oboseen.request.error;

import com.android.volley.VolleyError;

import obo.seen.oboseen.request.CustomRequest;


/**
 * Created by JR Aquino on 2/5/2017.
 */

public class SimpleNetworkErrorParser implements CustomRequest.NetworkErrorParser {
    @Override
    public VolleyError parseNetworkError(VolleyError err) {
        return err.getMessage() == null
                ? new VolleyError("Something happened", err)
                : err;
    }
}
