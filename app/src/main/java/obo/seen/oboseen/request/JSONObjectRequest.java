package obo.seen.oboseen.request;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JR Aquino on 2/4/2017.
 */

public class JSONObjectRequest extends CustomRequest<JSONObject> {

    public JSONObjectRequest(int method, String url,
                             NetworkErrorParser errorParser,
                             Response.Listener<JSONObject> listener,
                             Response.ErrorListener errorListener) {
        super(method, url, errorParser, listener, errorListener);
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        String json = new String(response.data);
        JSONObject responseBody = null;
        try {
            responseBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Response.success(
                responseBody,
                HttpHeaderParser.parseCacheHeaders(response));
    }
}
