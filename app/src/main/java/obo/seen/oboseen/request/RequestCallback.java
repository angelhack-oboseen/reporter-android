package obo.seen.oboseen.request;

/**
 * Created by JR Aquino on 2/7/2017.
 */

public interface RequestCallback<T> {
    void onResult(T result);
    void onFailed(String reason);
}
