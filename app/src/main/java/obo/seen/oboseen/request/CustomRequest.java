package obo.seen.oboseen.request;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JR Aquino on 11/30/2016.
 */

/**
 * Custom request that exposes mutators for the request header and parameters.
 * @param <T> type of expected response
 */
public abstract class CustomRequest<T> extends Request<T> {

    private Response.Listener<T> mResponseListener;
    private Map<String, String> mHeaders;
    private Map<String, String> mParams;
    private JSONObject jsonBody;
    private NetworkErrorParser mErrorParser;

    public CustomRequest(int method,
                         String url,
                         NetworkErrorParser errorParser,
                         Response.Listener<T> listener,
                         Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        mHeaders = new HashMap<>();
        mParams = new HashMap<>();
        mResponseListener = listener;
        mErrorParser = errorParser;
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }

    @Override
    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    @Override
    public byte[] getBody () throws AuthFailureError {
        if (jsonBody != null) {
            try {
                return jsonBody.toString().getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        }
        return super.getBody();
    }

    @Override
    public String getBodyContentType() {
        if (jsonBody != null) {
            return "application/json; charset=utf-8";
        }
        return super.getBodyContentType();
    }

    public JSONObject getJSONObjectResponseBody(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return new JSONObject(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setJSONBody(JSONObject body) {
        this.jsonBody = body;
    }
    /**
     * Sets the header field to a specified value
     * @param key the header field
     * @param value the header value
     */
    public CustomRequest setHeader(String key, String value) {
        mHeaders.put(key, value);
        return this;
    }

    /**
     * Sets the parameter field to the specified value
     * @param key the parameter field
     * @param value the parameter value
     */
    public void setParam(String key, String value) {
        mParams.put(key, value);
    }

    @Override
    protected void deliverResponse(T response) {
        mResponseListener.onResponse(response);
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        return mErrorParser.parseNetworkError(volleyError);
    }

    public interface NetworkErrorParser {
        VolleyError parseNetworkError(VolleyError err);
    }
}
