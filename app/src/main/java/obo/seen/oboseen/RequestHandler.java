package obo.seen.oboseen;

import android.annotation.SuppressLint;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


/**
 */
public class RequestHandler  {

    @SuppressLint("StaticFieldLeak")
    private static RequestHandler singleton;
    private Context mAppContext;
    private RequestQueue mRequestQueue;

    private RequestHandler(Context applicationContext) {
        mAppContext = applicationContext.getApplicationContext();
        mRequestQueue = getRequestQueue();
        mRequestQueue.start();
    }

    public static RequestHandler getInstance(Context context) {
        if (singleton == null) {
            singleton = new RequestHandler(context);
        }
        return singleton;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mAppContext);
        }
        return mRequestQueue;
    }

    public void handle(final Request request) {
        mRequestQueue.add(request);
    }
}
