package obo.seen.oboseen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import obo.seen.oboseen.request.JSONObjectRequest;
import obo.seen.oboseen.request.error.SimpleNetworkErrorParser;

public class RegisterActivity extends AppCompatActivity {

    EditText fName, lName, email, mobileNum;
    Button submitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        String id = getSharedPreferences("user", MODE_PRIVATE).getString("id", null);
        if (id != null) {
            Intent intent = new Intent(this, CaptureVideoActivity.class);
            startActivity(intent);
            finish();
        }
        fName = (EditText) findViewById(R.id.fName);
        lName = (EditText) findViewById(R.id.lName);
        email = (EditText) findViewById(R.id.email);
        mobileNum = (EditText) findViewById(R.id.mobile);
        submitBtn = (Button) findViewById(R.id.submitBtn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObjectRequest r = new JSONObjectRequest(Request.Method.POST, MainActivity.BASE_URL + "/register",
                        new SimpleNetworkErrorParser(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response.getString("id"));
                            getSharedPreferences("user", MODE_PRIVATE)
                                    .edit()
                                    .putString("id", response.getString("id"))
                                    .apply();
                            Intent intent = new Intent(RegisterActivity.this, CaptureVideoActivity.class);
                            startActivity(intent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                r.setParam("first_name", fName.getText().toString());
                r.setParam("last_name", lName.getText().toString());
                r.setParam("email", email.getText().toString());
                r.setParam("mobile_number", mobileNum.getText().toString());
                RequestHandler.getInstance(RegisterActivity.this).handle(r);
            }
        });
    }
}
